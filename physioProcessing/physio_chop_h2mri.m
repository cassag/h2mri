%  PHYSIO_CHOP_H2MRI: function to chop up the physiological data to create
%  noise regressors text files.
%
%  Usage: physio_chop_h2mri(matdir,ds, hr_lp, resp_lp) matdir is specified
%  by user interface if not specified. Specifies the directory of the mat
%  files created by acq2mat from bioread (https://github.com/uwmadison-chm/bioread)
%  with channel names confirmed by physio_plotChannels_h2mri.
%
%  Default values used for downsamle and filter are used if not supplied:
%       ds    = frequency (Hz) which the data is downsampled to (default = 50)
%       hr_lp = low pass frequency at which the cardiac data is filtered (default = 120 bpm)
%       resp_lp = low pass frequency at which the respiratory data is filtered (default = 60rpm).
%  NB these defaults have worked well in previous data sets, but may have
%  to be changed if too low. array of strings and is what the biopac
%  channel was called when you collected the data in the order
%  {'resp','pulse ox','scanner triggers'}, these are also the deafult
%  values. You can find this data by running acq2mat in a folder with the
%  acq data and then looking at the created mat file.

%  chanNames is what the biopac channel was called when the data were
%  collected. The above column names were generated from the biopac
%  configuration used for 7DP. These should be confirmed to match the
%  structure below for all participants processed using the function
%  "physio_potChannels_h2mri":
%      chanNames={'Skin Conductance','Trigger','Pulse','Respiratory
%      Effort'}
%  If the channel names do not match the above structure, they should be
%  amended in the variable below.

%  Original code developed by Michael Browning, Department of Psychiatry,
%  University of Oxford (michael.browning@psych.ox.ac.uk). Modified by
%  Wanjun Lin (wanjun.lin@psych.ox.ac.uk).
%
%  Adapted for processing of PERL "Faces" task by Cassandra Gould van Praag
%  (cassandra.gouldvanpraag@psych.ox.ac.uk), March 2020.
%  Modified to fit acq data structure created by bioread
%  (https://github.com/uwmadison-chm/bioread) as this is more stable in
%  extracting all data and across platforms (Mac/PC).
%  Distributed under CC-BY 4.0 License applied to the entire repository
%  https://git.fmrib.ox.ac.uk/cassag/h2mri

function physio_chop_h2mri(matdir,txtdir,ds, hr_lp, resp_lp,chanNames)

if nargin<2
    matdir='/Users/cassandragouldvanpraag/Library/Mobile Documents/com~apple~CloudDocs/Work/Studies/BRCSupport/7DP/physio-processing/data-mat';
    txtdir='/Users/cassandragouldvanpraag/Library/Mobile Documents/com~apple~CloudDocs/Work/Studies/BRCSupport/7DP/physio-processing/data-txt';
    
    % identify location of mat data (if not avaiable above for debigging)
    if ~exist('matdir','var')
        title_str = 'Select the directory containing matlab converted files';
        if ~ispc; menu(title_str,'OK'); end
        matdir = uigetdir('',title_str);
    end
    
    if ~exist('txtdir','var')
        title_str = 'Select the directory to store txt files for pnm';
        if ~ispc; menu(title_str,'OK'); end
        txtdir = uigetdir('',title_str);
    end
end

if ~isdir(matdir); error('Directory not found'); end

if nargin<2; ds=50; end
if nargin<3; hr_lp=2;end  % 120 bpm
if nargin<4; resp_lp=1;end % 60 rpm
%if nargin<5; col_types={'resp','pulse ox','scanner triggers'};end
if nargin<5; chanNames={'Skin Conductance','Respiratory Effort','Pulse','MRI Scanner'};end %** CGVP
% cd(matdir);

fn_lst = dir(fullfile(matdir,'*.mat'));


for i=1:length(fn_lst)
    
    fname = fn_lst(i).name;
    fpath = fullfile(fn_lst(i).folder,fn_lst(i).name);
    
    %     work = load(fname); %** bioread acq2mat doesn't save as acq
    tmp = load(fpath);
    work.acq = tmp;
    clear 'tmp'
    
    
    col_found = ones(size(chanNames)).*-99;
    physio = [];
    
    physiods = [];
    
    %iterate through the chanels to find relevantdata
    %     for j = 1:size(work.acq.hdr.per_chan_data,2)
    for j = 1:size(work.acq.channels,2) %** CGVP
        
        
        %     if strcmp(work.acq.hdr.per_chan_data(1,j).comment_text,'Skin
        %     Conductance') %** CGVP
        if strcmp({work.acq.channels{j}.name}, 'Skin Conductance')
            col_found(1) = j; %** CGVP
            chan_skin = j;
            physio_chan_skin = 1;
            %         elseif strcmp(work.acq.hdr.per_chan_data(1,j).comment_text,'Trigger')
        elseif strcmp({work.acq.channels{j}.name}, 'Respiratory Effort')
            col_found(2) = j;
            chan_resp = j;
            physio_chan_resp = 2;
            %         elseif strcmp(work.acq.hdr.per_chan_data(1,j).comment_text,'Pulse')
        elseif strcmp({work.acq.channels{j}.name}, 'Pulse')
            col_found(3) = j;
            chan_pulse = j;
            physio_chan_pulse = 3;
            %         elseif strcmp(work.acq.hdr.per_chan_data(1,j).comment_text,'Respiratory Effort')
        elseif strcmp({work.acq.channels{j}.name}, 'MRI Scanner')
            col_found(4) = j;
            chan_MRI = j;
            physio_chan_MRI = 4;
        end
    end
    
    
    if sum(col_found == -99)
        error('Unable to find data column')
    end
    
    % put physiological  data together and then downsample if necessary
    % physio data is repsiritory, cardiac, trigger, and skin conductance
    %     physio(:,1) = work.acq.data(:,col_found(1)); %** CGVP
    %     physio(:,2) = work.acq.data(:,col_found(2));
    %     physio(:,3) = work.acq.data(:,col_found(3));
    %     physio(:,4) = work.acq.data(:,col_found(4));
    physio(:,physio_chan_skin) = work.acq.channels{chan_skin}.data; %skin
    physio(:,physio_chan_resp) = work.acq.channels{chan_resp}.data; %resp
    physio(:,physio_chan_pulse) = work.acq.channels{chan_pulse}.data; %Pulse
    physio(:,physio_chan_MRI) = work.acq.channels{chan_MRI}.data; %MRI
    
    
    % this is number of miliseconds per sample (2 for 500 Hz 7DP data)
    %     ms_per_sample = work.acq.hdr.graph.sample_time; %**
    ms_per_sample = 1000/work.acq.samples_per_second;
    
    % remove data which occurs before first trigger
    % what a trigger looks like in the trigger data
    trigg = ones(10,1)*5;
%     idx_trigg = strfind(physio(:,2)',trigg');
    idx_trigg = strfind(physio(:,physio_chan_MRI)',trigg');
    first_trig = idx_trigg(1);
%   if first_trig > 500 % assumes 1000Hz sample rate
    start_buffer = 500*(work.acq.samples_per_second/1000); %create a 500ms buffer calculated from the sampling rate
    if first_trig > start_buffer    
        first_trig = first_trig-(500./ms_per_sample);  % NB collect 500ms before the first trigger
    else
        first_trig = 1;
    end
    physio = physio(first_trig:end,:);
    
    
    % remove data after end of task
    last_trig = idx_trigg(end-5);
    last_trig = last_trig+(3000./ms_per_sample); %collect 3 secs after the last trigger
    if last_trig<length(physio)
        physio=physio(1:last_trig,:);
    end
    
    %downsample to ds Hz
    
    
    
    %first trim sample down to the nearest integer multiple of downsampled
    %rate
    nsamps = size(physio,1);
%     new_ms_per_sample = 1000./ds;
	new_ms_per_sample = work.acq.samples_per_second./ds;
    conversion_ratio = new_ms_per_sample./ms_per_sample;
    
    if conversion_ratio ~= 1
        nsamps=floor(nsamps./conversion_ratio).*conversion_ratio;
        physio=physio(1:nsamps,:);
        
        % then downsample data
        for k=1:size(physio,2)
            physiods(:,k)=decimate(physio(:,k),conversion_ratio);
        end
        
    else
        physiods=physio;
    end
    % save the physio file as txt for PNM
    
    % NB converting the trigger into a logical vector using the following
    % cut offs and low pass filtering the respiratory data seems to help
    % PNM work better
    
    %*** CASS WORK FROM HERE  - COMPARE channel numbers in original to my
    %channel names
    
    physiods(:,physio_chan_MRI)=physiods(:,physio_chan_MRI)>4.9999;
    
    % filter respiratory data
    [z pfilt k]=butter(1,resp_lp/25,'low');
    [sos g]=zp2sos(z,pfilt,k);
    Hd=dfilt.df2tsos(sos,g);
%     physiods(:,1)=filter(Hd,physiods(:,4));
    physiods(:,physio_chan_resp)=filter(Hd,physiods(:,physio_chan_resp));
    
    % filter cardiac data
    [z pfilt k]=butter(1,hr_lp/25,'low');
    [sos g]=zp2sos(z,pfilt,k);
    Hd=dfilt.df2tsos(sos,g);
    physiods(:,physio_chan_pulse)=filter(Hd,physiods(:,physio_chan_pulse));
    
    [~,fname_processed,~] = fileparts(fname);
    fname_processed = fullfile(txtdir,(strcat(fname_processed,'_physio.txt')));
    
%   save([lower(processed_files{i}),'_physio.txt'],'physiods','-ascii');   %**
    save(fname_processed,'physiods','-ascii');
    
end
end
