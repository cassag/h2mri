%  PHYSIO_PLOTCHANNELS_H2MRI: Plots the channels of physiological data made
%  available after running acq2mat distributed by bioread (https://github.com/uwmadison-chm/bioread). This function should be run to:
%  1) confirm that the same channel set up was used for all participant; 2)
%  rename the trigger channel, which has a corrupted/non readable file name
%  in the biopac files acquired in the PERL 7DP study.
%
%  Running the function creates two plots per file contained in the
%  identified directory. The first plot is a full view of the full data
%  acquired. The second plot is a zoomed window of around 20 seconds (at
%  the assumed 500 Hz samling rate of the PERL 7DP faces task this code was
%  developed for). This zoomed window allows the user to confirm that the
%  shape of the waveform is as expected for the physiological (or scanner)
%  signal.
%
%  After plots are generated, the user is required to confirm that the
%  labels assigned to each channel are correct before moving onto the next
%  participant. If they are INCORRECT for any participant, the user should
%  note that file name and inspect the channel names in the mat file and
%  identify how they devliate from the expected structure of "chanNames"
%  used below.
%
%  chanNames is what the biopac channel was called when the data were
%  collected. The above column names were generated from the biopac
%  configuration used for 7DP. Running this script will allow to to confirm
%  whether the column headers are correct for your data. If any are found
%  to be incorrect, have a look at the channel names in the matlab files
%  created by acq2mat_h2mri. Read the channel names from the data here:
%     acq.data(:,1) = acq.hdr.per_chan_data(4).comment_text acq.data(:,2) =
%     acq.hdr.per_chan_data(1).comment_text acq.data(:,3) =
%     acq.hdr.per_chan_data(2).comment_text acq.data(:,4) =
%     acq.hdr.per_chan_data(3).comment_text
%  this data by running acq2mat in a folder with the acq data and then
%  looking at the created mat file.
%
%  Developed for processing of PERL "Faces" task by Cassandra Gould van
%  Praag (cassandra.gouldvanpraag@psych.ox.ac.uk), March 2020.
%  Distributed under CC-BY 4.0 License applied to the entire repository
%  https://git.fmrib.ox.ac.uk/cassag/h2mri

function physio_plotChannels_h2mri
dbstop if error

matdir='/Users/cassandragouldvanpraag/Library/Mobile Documents/com~apple~CloudDocs/Work/Studies/BRCSupport/7DP/physio-processing/data-mat';

chanNames={'Skin Conductance','Trigger','Pulse','Respiratory Effort'};

% what a trigger looks like in the trigger data
trigg = ones(10,1)*5;

% identify location of mat data (if not avaiable above for debigging)
if ~exist('matdir','var')
    title_str = 'Select the directory to store matlab converted files';
    if ~ispc; menu(title_str,'OK'); end
    matdir = uigetdir('',title_str);
end

% get a list of matlab files in that directory. Assumes all files in there
% will need to be processed.
fn_lst = dir(fullfile(matdir,'*.mat'));

% for each file in the list
for j=1:length(fn_lst)
    
    % get the filename from the dir structure
    fname = fn_lst(j).name;
    
    %load(fullfile(fn_lst(j).folder,fname));
    acq = load(fullfile(fn_lst(j).folder,fname)); %** acq2mat from bioread doesn't save as acq
    
    % plot the full data
    makeplot(chanNames,acq,fname)
    
    % find the trigger points
    % find the trigger channel
    for k = 1:size(acq.channels,2)
        match = reshape(strcmp({acq.channels{k}.name}, 'MRI Scanner'), size(acq));
        if match
            chan_MRI = k;
        end
    end
    
    %     idx_trigg = strfind(acq.data(:,2)',trigg');
    %     idx_trigg = strfind(acq.channels{chan_MRI}.data',trigg');
    idx_trigg = strfind(acq.channels{chan_MRI}.data,trigg');
    % set the minimal left limit to just before the first volume trigger
    xLeft = idx_trigg(1) - 1000;
    % set the right trigger to 10000 samples later (20 seconds at a sample
    % rate of 500 Hz)
    xRight = idx_trigg(1)+ 10000;
    
    % plot a window of the data around the 20 seconds from the first volume
    makeplot(chanNames,acq,strcat(fname,' - zoomed'),xLeft,xRight)
    
    renameChan = questdlg('Are the channels labelled correctly?','No','Yes');
    
    if strcmp(renameChan,'Yes')
        fprintf(' -- Saving .mat file.\n');
        save(fullfile(fn_lst(j).folder,fname),'acq');
    else
        fprintf(' -- WARNING: Please re-check the channel names in the acq mat file\n');
    end
    
    close all
    
    
end

end

function makeplot(chanName,acq,fname,xLeft,xRight)

figure('NumberTitle', 'off', 'Name', fname);


% subplot(4,1,1);
% plot(acq.data(:,1));
% legend(strcat(chanName{1},' (col1)'));
%
% subplot(4,1,2);
% plot(acq.data(:,2));
% legend(strcat(chanName{2},' (col2)'));
%
% subplot(4,1,3);
% plot(acq.data(:,3));
% legend(strcat(chanName{3},' (col3)'));
%
% subplot(4,1,4);
% plot(acq.data(:,4));
% legend(strcat(chanName{4},' (col4)'));


subplot(4,1,1);
plot(acq.channels{1}.data);
legend(acq.channels{1}.name);

subplot(4,1,2);
plot(acq.channels{2}.data);
legend(acq.channels{2}.name);

subplot(4,1,3);
plot(acq.channels{3}.data);
legend(acq.channels{3}.name);

subplot(4,1,4);
plot(acq.channels{4}.data);
legend(acq.channels{4}.name);

%if given X limits, rescale to those limits
if nargin > 3
    subplot(4,1,1);
    xlim([xLeft, xRight]);
    subplot(4,1,2);
    xlim([xLeft, xRight]);
    subplot(4,1,3);
    xlim([xLeft, xRight]);
    subplot(4,1,4);
    xlim([xLeft, xRight]);
end



end

