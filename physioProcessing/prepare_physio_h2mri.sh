#! /bin/bash
# script to generate physiological regressors
physio_dir=/home/michaelb/data/bupropion/physio/
subject=`echo $1`
visit=`echo $2`
wd=`pwd`
infile=${physio_dir}${subject}/visit_${visit}/${subject}_faces_v${visit}_physio.txt
outfile=${physio_dir}${subject}/visit_${visit}/${subject}_faces_v${visit}_physio_confound
outdir=${physio_dir}${subject}/visit_${visit}/
file=${subject}_faces_v${visit}_physio_confound
pnm_stage1 -i ${infile} -o ${outfile} -s 50 --tr=2.71 --smoothcard=0.1 --smoothresp=0.1 --resp=1 --cardiac=2 --trigger=3 --rvt #--pulseox_trigger
#make a matlab ready file
 sed -e s'@\\n.* @@'  -e s'@\"@@' -e '/function/d' -e '/return/d' -e s'@}.*@@' -e s'@\\n.*@@' <${outfile}_pnm.js >${outfile}_matlab.txt
# put in the extra lines into stage 2 needed to run later analyses
echo "${outfile}_pnm_stage3" >> "${outfile}_pnm_stage2"
# create stage 3
echo '#!/bin/sh' >"${outfile}_pnm_stage3"
echo "${FSLDIR}/bin/pnm_evs -i /home/michaelb/data/bupropion/raw_data/${subject}/visit_${visit}/${subject}_${visit}_faces.nii.gz -c ${outfile}_card.txt -r ${outfile}_resp.txt -o ${outfile} --tr=2.71 --oc=4 --or=4 --multc=2 --multr=2 --sliceorder=down --rvt=${outfile}_rvt.txt --rvtsmooth=10 " >> "${outfile}_pnm_stage3"
echo "cd ${outdir}">> "${outfile}_pnm_stage3"
echo 'find '"${outdir}${file}"'ev0*  > '"${outfile}"'_evlist.txt' >> "${outfile}_pnm_stage3"
echo "cd ${wd}">> "${outfile}_pnm_stage3"
chmod a+x "${outfile}_pnm_stage3"
