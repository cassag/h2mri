%  PHYSIO_PLOTCHANNELS_H2MRI: Plots the channels of physiological data made
%  available after running acq2mat distributed by bioread (https://github.com/uwmadison-chm/bioread). This function should be run to:
%  1) confirm that the same channel set up was used for all participant; 2)
%  rename the trigger channel, which has a corrupted/non readable file name
%  in the biopac files acquired in the PERL 7DP study.
%
%  Running the function creates two plots per file contained in the
%  identified directory. The first plot is a full view of the full data
%  acquired. The second plot is a zoomed window of around 20 seconds (at
%  the assumed 500 Hz samling rate of the PERL 7DP faces task this code was
%  developed for). This zoomed window allows the user to confirm that the
%  shape of the waveform is as expected for the physiological (or scanner)
%  signal.
%
%  After plots are generated, the user is required to confirm that the
%  labels assigned to each channel are correct before moving onto the next
%  participant. If they are INCORRECT for any participant, the user should
%  note that file name and inspect the channel names in the mat file and
%  identify how they devliate from the expected structure of "chanNames"
%  used below.
%
%  chanNames is what the biopac channel was called when the data were
%  collected. The above column names were generated from the biopac
%  configuration used for 7DP. Running this script will allow to to confirm
%  whether the column headers are correct for your data. If any are found
%  to be incorrect, have a look at the channel names in the matlab files
%  created by acq2mat_h2mri. Read the channel names from the data here:
%     acq.data(:,1) = acq.hdr.per_chan_data(4).comment_text acq.data(:,2) =
%     acq.hdr.per_chan_data(1).comment_text acq.data(:,3) =
%     acq.hdr.per_chan_data(2).comment_text acq.data(:,4) =
%     acq.hdr.per_chan_data(3).comment_text
%  this data by running acq2mat in a folder with the acq data and then
%  looking at the created mat file.
%
%  Developed for processing of PERL "Faces" task by Cassandra Gould van
%  Praag (cassandra.gouldvanpraag@psych.ox.ac.uk), March 2020.
%  Distributed under CC-BY 4.0 License applied to the entire repository
%  https://git.fmrib.ox.ac.uk/cassag/h2mri

%  Adapted from Wanjun's check_pnm_txt.m

function physio_plotChannels_txt_h2mri
dbstop if error

txtdir='/Users/cassandragouldvanpraag/Library/Mobile Documents/com~apple~CloudDocs/Work/Studies/BRCSupport/7DP/physio-processing/data-txt';

chanNames={'Skin Conductance','Respiratory Effort','Pulse','MRI Scanner'}; %** CGVP


% identify location of mat data (if not avaiable above for debigging)
if ~exist('txtdir','var')
    title_str = 'Select the directory containing the txt files for pnm';
    if ~ispc; menu(title_str,'OK'); end
    txtdir = uigetdir('',title_str);
end



% get a list of matlab files in that directory. Assumes all files in there
% will need to be processed.
fn_lst = dir(fullfile(txtdir,'*.txt'));

% for each file in the list
for j=1:length(fn_lst)
    
    % get the filename from the dir structure
    fname = fn_lst(j).name;
    fpath = fullfile(fn_lst(j).folder,fname);
    
    fid = fopen(fpath);
    
    dat = textscan(fid,'%f %f %f %f');
    fclose(fid);
    
    % plot the full data
    makeplot(chanNames,dat,fname)
    
    renameChan = questdlg('Filtering looks ok?','No','Yes');
    
    if strcmp(renameChan,'Yes')
        fprintf(' -- Saving plot. \n');
        save(fullfile(fn_lst(j).folder,fname),'acq');
    else
        fprintf(' -- WARNING: Please review filtering.\n');
        fprintf(' -- Saving plot. \n');
    end
    
    [~,figname,~] = fileparts(fname)
    figname = fullfile(txtdir,(strcat(figname,'.png')))
    
    saveas(f,figname)
    
    close all
    
    
end

end

function makeplot(chanName,dat,fname)

f = figure('NumberTitle', 'off', 'Name', fname);

subplot(4,1,1);
plot(dat{1,1}(:,1));
legend(chanName{1});

subplot(4,1,2);
plot(dat{1,2}(:,1));
legend(chanName{2});

subplot(4,1,3);
plot(dat{1,3}(:,1));
legend(chanName{3});

subplot(4,1,4);
plot(dat{1,4}(:,1));
legend(chanName{4});


end

