#!/bin/bash

# Loops through the participant list to create the dicominfo.tsv file

# do your dicom directory names have spaces in? If so, keep in lines 3 & 4 below (https://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html), if not, comment out.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
# unpack individual sequence folders into one space for heudiconv (handles better than individual folders)
# h2mri-update: dataRoot should point to your study folder
dataRoot="/vols/Scratch/brc_em/test"

# h2mri-update: enter your participant ids in a list as below
for subject in 011 012 013; do
    # expand the dicom folders as retrieved from calpendo into a single directory
    # ls -1d will list all the directories (1 level deep) under the particpant folder
    # each directory will then be $d
    # checks first if subdirectories exist, so we dont get an error message when can't unpack
    if [ "$(find $dataRoot/sourcedata/dicom/$subject -maxdepth 1 -type d -printf 1 | wc -m)" -gt 1 ]; then
        for d in $(ls -1d $dataRoot/sourcedata/dicom/cassBIDSCheck/$subject/*/); do
        echo "-- Unpacking $d"
        # move the contents of $d into the subject directory
        mv $d/* $dataRoot/sourcedata/dicom/$subject/
        # remove $d
        rmdir $d
        done
    fi

    # This will use an image of heudiconv 0.5.4 pulled from dockerhub on 11/03/2019
    # via singularity pull --name singularityContainer_heudiconv_054.sif docker://nipy/heudiconv:0.5.4
    # It was included in the h2mri gitlab repository
    # tried pulling the "latest" version, but "debian" was the only tag available which wasn't "unstable" https://hub.docker.com/r/nipy/heudiconv/tags

    # h2mri-update: sepecify the correct path for your study folder
    # (unfortunately this bind and spcification of the singularity container cannot take a value of $dataRoot as input)

    singularity run -B \
    /vols/Scratch/brc_em/test:/base \
    /vols/Scratch/brc_em/test/code/BIDSConversion/singularityContainer_heudiconv_054.sif \
    -d /base/sourcedata/dicom/{subject}/* \
    -o /base/sourcedata/BIDS/ \
    -f convertall \
    -s $subject  \
    -c none \
    --overwrite

    # deleting the pxID.edit.txt and pxID.auto.txt files so these are not automatically read when running the heuristic
    # see https://neurostars.org/t/overwriting-heudiconv-sub-auto-txt-and-sub-edit-txt/4172
    f_edit=$dataRoot/sourcedata/BIDS/.heudiconv/$subject/info/$subject.edit.txt
    f_auto=$dataRoot/sourcedata/BIDS/.heudiconv/$subject/info/$subject.auto.txt

    # only runs if we find the files, i.e. if heudiconv happend (avoids error message)
    if [ -f $f_edit ] && [ -f $f_auto ]; then
        echo "-- Deleting $f_edit"
        rm $f_edit
        echo "-- Deleting $f_auto"
        rm $f_auto
    fi

done
