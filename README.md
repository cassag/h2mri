# h2mri - README.md
This repositpory contains the example code used in the [Psychopharmacology and Emotion Research Laboratory](https://www.psych.ox.ac.uk/research/psychopharmacology-and-emotion-research-laboratory) h2MRI workshops.

Also included in this repository is a [training guide](https://git.fmrib.ox.ac.uk/cassag/h2mri/-/blob/master/HowToMRI_published.pdf) (live document at [www.bit.ly/BRCEM-h2mri](https://docs.google.com/document/d/168cutrDOvZOh9GOJPaJqKeJMGULuE-c4Y3QbPvb_Igw/edit?usp=sharing)) designed to be folowed by researchers in BRC-EM.

## CITATION

[![DOI:10.5281/zenodo.7528600](https://zenodo.org/badge/DOI/10.5281/zenodo.7528600.svg)](https://doi.org/10.5281/zenodo.7528600)

Please cite as:

Gould van Praag, Cassandra, (2019). BRC Experimental Medicine Theme Transparent and reproducible fMRI analysis guide. Zenodo. https://doi.org/10.5281/zenodo.7528600


## LICENCE
Documentation shared under a CC-BY-4.0. Code shared under an Apache 2.0 License.

## ACKNOWLEDGEMENTS

Novel code is primarily wrappers around open MRI analysis libraries. Specific use is made of:

- BIDS (https://doi.org/10.1038/sdata.2016.44)
- HeuDiConv (https://doi.org/10.5281/zenodo.1012598)
- Fmriprep (https://doi.org/10.1038/s41592-018-0235-4)
- MRIQC (https://doi.org/10.1371/journal.pone.0184661)

Thanks to the participants on this training opportunity for their support and feedback while creating and running this material.




## Topics covered with code available:

See the tutorial guide for using this code: HowToMRI_published.pdf

1. renamePxFolders      - Section 6.5
2. BIDSConversion       - Section 7
3. mriqc                - Section 8
4. fmriprep             - Section 9
5. EV generation        - Section 10 (online giude only: www.bit.ly/BRCEM-h2mri)
6. First level analysis - Section 11 (online giude only: www.bit.ly/BRCEM-h2mri)

Further code examples may be made available as the workshops progress.

## Cass' bash profile
Below are a few modifications which might make your termainal a bit easier to work with. The following descibes how to edit your bash profile and make these changes. 

### 1. open your bash profile in nano
Once logged into jalapeno, navigate to your home directory:
`>> cd ~`
List hidden files using `ls -la` and you will see a file called `.bash_profile`. Open that file in nano:
`>> nano .bash_profile`

### 2. Edit your bash profile
This file is automatically filled with lots of useful things by the IT administrators (defaults) and FSL when you install it. We'll not change any of that!

Scroll all the way to the bottom of your profile and add the lines below:
```
alias lsl='ls -l'
alias lsla='ls -la'
#https://superuser.com/questions/601181/how-to-display-current-path-in-command-prompt-in-linuxs-sh-not-bash
#export PS1='$(whoami)@$(hostname):$(pwd)'
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
```

The first two lines create short cut "aliases" which I use a lot to save typing seconds! Once this line is in your bash profile, when you type `lsl` in the terminal it will be interprestted as `ls -l`. You can creat other aliases for functions you use and add them to your profile.

Line 3 (starting with a `#https`) is the refernece for where I found the final line. `#` means this line is a comment.

Line 4 (starting `#export`) is where I've commented out the original settings which define what information should be shown at the command prompt on your terminal. **Please look through your profile and see if this line (uncommented) exists anywhere. If it does, comment it out by putting a `#` in front of it.**

The final line gives a new setting of what should be shown at the terminal command prompt. Please see the https reference for a description pf what this inlcudes. 

### 3. Save and reload you profile
Use the propmets in nano to save your edited profile (look to the bottom of the screen).

Your profile is loaded every time you open a new terminal, so you will not see the changes immediately. Open a new terminal (or tab) to start a session with the new changes. 

Remember that these changes will only be opresent for you bash profile on jalapeno. You might like to go through the same process for your bash profile on you local computer. 


