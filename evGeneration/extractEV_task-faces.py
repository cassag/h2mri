# extractEV.py
# extractEV.py is a script that extracts event timings from psychopy files from each scan
# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# https://pbpython.com/pathlib-intro.html
# pathlib allows you to specify something as a path so you can then do more things with it because it doesn't think it's a string
from pathlib import Path
import time
import numpy as np

# dataRoot = Path.cwd() / "data"
# h2mri: edit the below to point to your sourcedata folder
dataRoot = '/vols/Scratch/brc_em/7DP/sourcedata'
dataRoot = Path(dataRoot)

# h2mri: edit the below for the task you want to analyse and how the task csv files can be identified
task = 'faces'
resFileIdentifier = '*FERT*.csv'

path_rawEvents = os.path.join(dataRoot,'events','rawData','task-' + task)
path_rawEvents = Path(path_rawEvents)
path_rawEvents.mkdir(parents=True, exist_ok=True)

path_evOutput =  os.path.join(dataRoot,'events','evFiles','task-' + task)
path_evOutput = Path(path_evOutput)
path_evOutput.mkdir(parents=True, exist_ok=True)

fList = []
# print(fList)
# input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
# glob looks for *FERT*.csv in specified path which is dataRoot in this case
# uncomment print, this will then print each of the files with *FERT* it finds
# for each loop, it will appedn the name of the file, the parent file and time etc created. fList becomes this big
# array that contains all this information
for i in path_rawEvents.glob(resFileIdentifier):
    # print(i.name)
    # input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
    fList.append((i.name, i.parent, time.ctime(i.stat().st_ctime)))

# creating dataframe from the fList array
columns = ["File_Name", "Parent", "Created"]
df = pd.DataFrame.from_records(fList, columns=columns)
# print(df.head())
# splitext - Split the pathname path into a pair (root, ext) such that root + ext == path, and ext is empty or begins
# with a period and contains at most one period. Leading periods on the basename are ignored; splitext('.cshrc') returns ('.cshrc', '').
# takes filename and adds EV to it (below)
# this for loop pulls out info based on the file names in the DataFrame
# it pulls out the PxID and creates a version of the filename without the .csv extension
for index, p in df.iterrows():
    path_behavDat = path_rawEvents.joinpath(p.File_Name)
    print('\n -- Task results file:  ' + p.File_Name)

    fName_rootExt = os.path.splitext(p.File_Name)
    fName_root = fName_rootExt[0]

    # returns string before first split char (0 index)
    pxID = p.File_Name.split('_',1)[0]
    print(' -- pxID:               ' + pxID)
    # renames the columns in the csv file existing in the pandas dataframe
    # renamed them to make any dots in the column headers into underscores to make it nicer for us :)
    behavDat = pd.read_csv(path_behavDat)
    behavDat.rename(columns={'loop_block.thisIndex':'loop_block_index',
                            'key_trialResp1.corr':'key_trialResp1_corr'},
                            inplace=True)

    # dropna gets rids of all the empty cells in the time_voll column because there should only be 1 for each volume
    t_vol1 = behavDat.time_vol1.dropna()
    # reports length of column, if it's more than 1 (it shouldn't be) then it will print this warning
    if len(t_vol1) > 1:
        print(' -- WARNING: multiple time_vol1 values found. Please check file.')
        continue

    t_vol1 = t_vol1.item()

    print(' -- Time vol 1(s):      ' + str(t_vol1))

    listEmo = behavDat['im_emotion'].unique()
    listEmo = [x for x in listEmo if str(x) != 'nan']
# this part splits the file name, removes the tail (.csv), adds _ev to the head and changes the tail to .txt
    for e in listEmo:

        fName_ev = os.path.join(path_evOutput,'sub-' + pxID + '_task-' + task + '_ev-' + e + '.txt')
        # print(fName_ev)

        tmp = ["Onset", "Duration", "Weight"]
        ev_emo = pd.DataFrame(columns=tmp)

        print(' -- Condition:          ' + e)

        block_emo = behavDat[behavDat.im_emotion == e]
        # print(block_emo)

        nBlocks = block_emo['loop_block_index'].unique()
        # print(nBlocks)

        time_blockStart = np.array([])
        time_blockEnd   = np.array([])
        time_blockDur   = np.array([])

        for b in nBlocks:

            # print('\n -- Raw block timings:\n')
            block_b = block_emo[block_emo.loop_block_index == b]
            block_b = block_b[['im_emotion','time_trialBegin','time_trialEnd','key_trialResp1_corr']]
            # print(block_b)

            # print('\n -- Corrected block timings:\n')
            block_b.time_trialBegin = block_b.time_trialBegin - t_vol1
            block_b.time_trialEnd = block_b.time_trialEnd - t_vol1
            # print(block_b)

            tmp_s = block_b.time_trialBegin.min()
            tmp_e = block_b.time_trialEnd.max()
            tmp_d = tmp_e - tmp_s

            time_blockStart = np.append(time_blockStart,tmp_s)
            time_blockEnd   = np.append(time_blockEnd,tmp_e)
            time_blockDur   = np.append(time_blockDur,tmp_d)

            # print('\n')
            # print(time_blockStart)
            # print(time_blockEnd)
            # print(time_blockDur)

        ev_emo.Onset    = time_blockStart
        ev_emo.Duration = time_blockDur
        ev_emo.Weight = 1.000

        # print('\n -- Calculated EVs: \n')
        # print(ev_emo)

        print(' -- Saving to:            ' + fName_ev)

        # https://stackoverflow.com/questions/31247198/python-pandas-write-content-of-dataframe-into-text-file
        np.savetxt(fName_ev, ev_emo.values, fmt='%1.3f', delimiter='\t')

        # input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
