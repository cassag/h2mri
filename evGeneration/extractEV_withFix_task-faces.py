# extractEV.py

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd

# https://pbpython.com/pathlib-intro.html
from pathlib import Path
import time
import numpy as np

# where the scanner results files are
dataRoot = Path.cwd() / "PEP_Faces_Data"
dataRoot = Path(dataRoot)

# where the ve files will be saved
evRoot = Path.cwd() / "evFiles_withFixation_testAcc"
evRoot.mkdir(parents=True, exist_ok=True)

# list of files to process
fList = []

# arrays for storring accuracy
# for all data
acc_allPx = []
# for individual block data
acc_happ = []
acc_fear = []

# data frame column headers for saveing/printing ev results
evColHead = ["Onset", "Duration", "On"]

# collect list of files to process
for i in dataRoot.glob('*FERT*.csv'):
    # print(i.name)
    fList.append((i.name, i.parent, time.ctime(i.stat().st_ctime)))

columns = ["File_Name", "Parent", "Created"]
df = pd.DataFrame.from_records(fList, columns=columns)
# print(df.head())

# for each file (assume one per participant)
for index, p in df.iterrows():

    # create fresh arrays to score accuracy for each participant, as we are building them with append
    # and don't want to accidentally stick new data onto old arrays
    acc_px = []
    acc_px_emo = []

    # --------------------------------------------------------------------------
    # get/set the files names for this participant

    # get the name of the file being processed
    path_bevavDat = dataRoot.joinpath(p.File_Name)
    print('\n')
    print(' -- Task results file:            ' + p.File_Name)

    # now going to pull some useful information out of the file name and create an ev file name
    # 'split' the file name at the "_" character to get the first part, which we know to be pxID
    pxID = p.File_Name.split('_',1)[0]
    print(' -- pxID:                         ' + pxID)

    # for consistency, we're going to save the new ev file with the same name as the file it was created from.
    # the new file name will be the same as the old, with "condition" added on the end, and a different file extension
    # first get rid of the file extension. "splitext" splits the path name into a pair of root([0]) and ext([1])
    # other other midification (i.e. add "condition") will happen later on when we have defined the condition
    fName_split = os.path.splitext(p.File_Name)
    fName_root = fName_split[0]

    # --------------------------------------------------------------------------
    # begin processing the data!

    # read the data in
    behavDat = pd.read_csv(path_bevavDat)
    # rename some column headers form the original csv file as they have horrible dots in and we don't like random dots! (they mean something in python)
    behavDat.rename(columns={'loop_block.thisIndex':'loop_block_index',
                            'key_trialResp1.keys':'key_trialResp1_keys',
                            'key_trialResp1.corr':'key_trialResp1_corr'},
                            inplace=True)

    # get the time of volume 1.
    # this column of the data *should* only have one value in, so we use dropna to "drop" all the "nan" values
    t_vol1 = behavDat.time_vol1.dropna()
    # It's important that we really do only have one value for this time, as this is how we will sync our behaviour with the acquired MRI volumes
    # check to see if there is only one value found after dropna by checking the length of this variable.
    # Will skip to next participant if len>1
    if len(t_vol1) > 1:
        print(' -- WARNING: multiple time_vol1 values found. Please check file.')
        continue

    # if all good, take the item at location0 into a single value
    t_vol1 = t_vol1.iloc[0]
    print(' -- Time vol 1(s):                ' + str(t_vol1))

    # --------------------------------------------------------------------------
    # process the fixation block timings

    cond = 'fix'
    print(' -- Condition:                    ' + cond)

    # create the file name for the fixation ev
    fName_ev = os.path.join(evRoot,fName_root + '_ev_' + cond + '.txt')
    # create a data frame to store the fixation timings
    ev_fix = pd.DataFrame(columns=evColHead)

    # pull the timings out of the full behavioural results file
    # remove the empty cells in time_restBegin	and time_restEnd
    # then drop the empty cells
    time_fix = behavDat[['time_restBegin','time_restEnd']]
    time_fix = time_fix.dropna(how='all')

    # add the timings to the ev df, with the start time adjusted for the time of vol1
    ev_fix.Onset = time_fix.time_restBegin - t_vol1
    ev_fix.Duration = time_fix.time_restEnd - time_fix.time_restBegin
    ev_fix.On = 1

    # save out the ev file
    print(' -- Saving to:                    ' + fName_ev)
    np.savetxt(fName_ev, ev_fix.values, fmt='%1.3f', delimiter='\t')

    # --------------------------------------------------------------------------
    # Process the emotion block

    # (did this second as it's a little bit fiddlier and loopy, so got fixation out of the way first!)

    # generate a lit of the emotion names present in the file
    # find the unique values (words) in that column
    # then remove the unique "nan" (this is just a loop written in a line)
    listEmo = behavDat['im_emotion'].unique()
    listEmo = [x for x in listEmo if str(x) != 'nan']

    for cond in listEmo:
        print(' -- Condition:                    ' + cond)
        # create the file name to save into
        fName_ev = os.path.join(evRoot,fName_root + '_ev_' + cond + '.txt')
        # dataframe for collecting the timings
        ev_emo = pd.DataFrame(columns=evColHead)

        # find the data where the contents of the im_emotion column matches the condition we're working in
        block_emo = behavDat[behavDat.im_emotion == cond]

        # count how many blocks of data we have (we're going to loop through these next)
        nBlocks = block_emo['loop_block_index'].unique()

        # create some fresh arrays to store timings for each block
        time_blockStart = np.array([])
        time_blockEnd   = np.array([])
        time_blockDur   = np.array([])

        for b in nBlocks:

            # create some fresh arrays to store accuracy for each block
            acc = []
            acc_fear = []
            acc_happ = []

            # find the data which matches the block we're working on
            # (note both the psychopy output and this code will expect the first block to be "0")
            block_b = block_emo[block_emo.loop_block_index == b]
            # get just the columns we're interested in for timings and accuracy
            block_b = block_b[['im_emotion','time_trialBegin','time_trialEnd','key_trialResp1_keys','key_trialResp1_corr']]

            # ------------------------------------------------------------------
            # # Process the emotion block timings

            # correct the block timings for the time of vol1
            block_b.time_trialBegin = block_b.time_trialBegin - t_vol1
            block_b.time_trialEnd = block_b.time_trialEnd - t_vol1
            # find the minimum value for time a trial started (i.e. the start of the first trial)
            tmp_s = block_b.time_trialBegin.min()
            # find the maximum value for time a trial started (i.e. the end of the last trial)
            tmp_e = block_b.time_trialEnd.max()
            # difference the above for duration
            tmp_d = tmp_e - tmp_s

            # append these values to the timing arrays for this condition
            time_blockStart = np.append(time_blockStart,tmp_s)
            time_blockEnd   = np.append(time_blockEnd,tmp_e)
            time_blockDur   = np.append(time_blockDur,tmp_d)

            # ------------------------------------------------------------------
            # Process the emotion block accuracies

            # total accuracy scores
            score = block_b.key_trialResp1_corr.sum()
            ntrials = len(block_b.key_trialResp1_corr)
            acc = (score/ntrials)*100

            # accuracy score adjustment for no response trials
            # find the rows where key_resp = "None" and cont the number of "none" trials
            trialRespNone = block_b[block_b.key_trialResp1_keys == "None"]
            ntrials_respNon = len(trialRespNone)
            ntrials_adj = ntrials-ntrials_respNon

            # if score > 0:
            #     # acc_noRespAdj = (score/(ntrials-ntrials_respNon))*100
            #     acc_noRespAdj = (score/ntrials_adj)*100
            # else:
            #     # acc_noRespAdj = float('nan')
            #     acc_noRespAdj = 0

            # some vals for debugging
            # print('\n')
            # print(' ** block:              ' + str(b))
            # print(' ** score:              ' + str(score))
            # print(' ** n trials:           ' + str(ntrials))
            # print(' ** Raw acc:            ' + str((score/ntrials)*100))
            # print(' ** n no resp:          ' + str(ntrials_respNon))
            # print(' ** n trials adj:       ' + str(ntrials_adj))
            # print(' ** Adj acc:            ' + str((score/ntrials_adj)*100))

            # assign accuracy to participant array for appropriate condition
            if cond == 'fear':
                # acc_px_emo.append({'ntrials_fear': ntrials, 'score_fear': score, 'acc_fear': acc, 'nNoResp_fear': ntrials_respNon, 'acc_fear_adjNoResp':acc_noRespAdj,'ntrials_adj_fear':ntrials_adj})
                acc_px_emo.append({'ntrials_fear': ntrials, 'score_fear': score, 'nNoResp_fear': ntrials_respNon, 'ntrials_adj_fear':ntrials_adj})
            elif cond == 'happy':
                # acc_px_emo.append({'ntrials_happ': ntrials, 'score_happ': score, 'acc_happ': acc, 'nNoResp_happ': ntrials_respNon, 'acc_happ_adjNoResp':acc_noRespAdj,'ntrials_adj_happ':ntrials_adj})
                acc_px_emo.append({'ntrials_happ': ntrials, 'score_happ': score, 'nNoResp_happ': ntrials_respNon, 'ntrials_adj_happ':ntrials_adj})


        # ----------------------------------------------------------------------
        # drop out of block loop and back into condition to collate timings
        # Note saving one file per condition, so doing timings now (but not accuracies), as we will be doing something with accuracies per particpant rather than condition

        # take to stored timings into a single dataframe so they are eassier to print out
        ev_emo.Onset    = time_blockStart
        ev_emo.Duration = time_blockDur
        ev_emo.On = 1

        print(' -- Saving to:                    ' + fName_ev)
        # https://stackoverflow.com/questions/31247198/python-pandas-write-content-of-dataframe-into-text-file
        np.savetxt(fName_ev, ev_emo.values, fmt='%1.3f', delimiter='\t')

    # --------------------------------------------------------------------------
    # drop out of block loop (into participant loop) to process accuracies

    # we've been collating all the accuracy information over all conditions and blocks.
    # now build a sinlge accuracy score per condition

    # building the accuracy data
    dfAcc_px = pd.DataFrame(acc_px_emo)
    # print(dfAcc_px)

    ntrials_fear_tot = dfAcc_px.ntrials_fear.sum()
    ntrials_happ_tot = dfAcc_px.ntrials_happ.sum()

    score_fear_tot = dfAcc_px.score_fear.sum()
    score_happ_tot = dfAcc_px.score_happ.sum()

    # acc_fear_mean = dfAcc_px.acc_fear.mean(skipna=True)
    # acc_happ_mean = dfAcc_px.acc_happ.mean(skipna=True)
    acc_fear_mean = (score_fear_tot/ntrials_fear_tot)*100
    acc_happ_mean = (score_happ_tot/ntrials_happ_tot)*100

    noresp_fear_tot = dfAcc_px.nNoResp_fear.sum()
    noresp_happ_tot = dfAcc_px.nNoResp_happ.sum()

    ntrials_adj_fear = dfAcc_px.ntrials_adj_fear.sum()
    ntrials_adj_happ = dfAcc_px.ntrials_adj_happ.sum()

    # acc_fear_mean_adjNoResp = dfAcc_px.acc_fear_adjNoResp.mean(skipna=True)
    # acc_happ_mean_adjNoResp = dfAcc_px.acc_happ_adjNoResp.mean(skipna=True)
    acc_fear_mean_adjNoResp = (score_fear_tot/ntrials_adj_fear)*100
    acc_happ_mean_adjNoResp = (score_happ_tot/ntrials_adj_happ)*100

    perc_noresp_fear = (noresp_fear_tot/ntrials_fear_tot)*100
    perc_noresp_happ = (noresp_happ_tot/ntrials_happ_tot)*100



    print(' -- N trials fear total:          ' + str(ntrials_fear_tot))
    print(' -- N trials happ total:          ' + str(ntrials_happ_tot))

    print(' -- Score fear total:             ' + str(score_fear_tot))
    print(' -- Score happ total:             ' + str(score_happ_tot))

    print(' -- Accuracy fear mean:           ' + str(acc_fear_mean))
    print(' -- Accuracy happ mean:           ' + str(acc_happ_mean))

    print(' -- No resp fear total:           ' + str(noresp_fear_tot))
    print(' -- No resp happ total:           ' + str(noresp_happ_tot))

    print(' -- Accuracy fear mean adjusted:  ' + str(acc_fear_mean_adjNoResp))
    print(' -- Accuracy happ mean adjusted:  ' + str(acc_happ_mean_adjNoResp))

    print(' -- Percent no resp fear:         ' + str(perc_noresp_fear))
    print(' -- Percent no resp happ:         ' + str(perc_noresp_happ))

    print(' -- N trials adj fear total:      ' + str(ntrials_adj_fear))
    print(' -- N trials adj happ total:      ' + str(ntrials_adj_happ))


    # now need to add these to a big collection of all px
    acc_allPx.append({'pxID':               pxID,
                      'ntrials_fear':       ntrials_fear_tot,
                      'ntrials_happ':       ntrials_happ_tot,
                      'score_fear':         score_fear_tot,
                      'score_happ':         score_happ_tot,
                      'acc_fear':           acc_fear_mean,
                      'acc_happ':           acc_happ_mean,
                      'nNoResp_fear':       noresp_fear_tot,
                      'nNoResp_happ':       noresp_happ_tot,
                      'ntrials_adj_fear':    ntrials_adj_fear,
                      'ntrials_adj_happ':    ntrials_adj_happ,
                      'acc_fear_adjNoResp': acc_fear_mean_adjNoResp,
                      'acc_happ_adjNoResp': acc_happ_mean_adjNoResp,
                      'perc_noresp_fear':   perc_noresp_fear,
                      'perc_noresp_happ':   perc_noresp_happ})

    # input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')


# convert accuracy data to df and print once
final_acc_allPx = pd.DataFrame(acc_allPx)

print('\n\n')

# print(final_acc_allPx)

fName_acc = os.path.join(evRoot,'accuracyScores_allPx.csv')

print(' -- Saving all accuracy results to:                    ' + fName_acc)
# https://stackoverflow.com/questions/31247198/python-pandas-write-content-of-dataframe-into-text-file
# np.savetxt(fName_acc, final_acc_allPx.values, fmt='%1.3f', delimiter='\t')
final_acc_allPx.to_csv(fName_acc)
