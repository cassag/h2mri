# replaceRegFiles.py

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# https://pbpython.com/pathlib-intro.html
from pathlib import Path
import time
import numpy as np
from shutil import copyfile

# h2mri: edit the below to point to your sourcedata folder
dataRoot = '/vols/Scratch/brc_em/SDLamo/sourcedata'
# dataRoot = '/Users/cassandragouldvanpraag/Documents/mount/scratch/cassag/mycode/h2mri/firstLevel/sourcedata'
dataRoot = Path(dataRoot)

# h2mri: edit the below to match the task you are modeling. This should match the name of the task that was assigned in BIDS
task = 'faces'

# glob for directories only, i.e. the list of participants who have been feat'd
for i in dataRoot.glob('feat/task-' + task + '/*/'):
    # print(i)
    pxID = (i.name)
    print(' -- Processing participant: ' + pxID)
    path_pxFeatDir = dataRoot.joinpath('feat/task-' + task , pxID)
    path_pxRegDir = path_pxFeatDir.joinpath('reg')
    if Path.exists(path_pxRegDir):
        print(' -- Found Directory' + str(path_pxRegDir))
        # copy a template idententy matrix from the FSL directory and use it to replace our participants "calculated" transformation matrix
        print(' -- Replacing FEAT 6DOF realignment matrix with identity matrix')
        copyfile(str(path_pxRegDir) + '/example_func2standard.mat' , str(path_pxRegDir) + '/example_func2standard_fromFeatPreproc.mat')
        copyfile('/opt/fmrib/fsl/etc/flirtsch/ident.mat' , str(path_pxRegDir) + '/example_func2standard.mat')

        print(' -- Replacing reg standard with mean func')
        copyfile(str(path_pxRegDir) + '/standard.nii.gz' , str(path_pxRegDir) + '/standard_fromFeatPreproc.nii.gz')
        copyfile(str(path_pxFeatDir) + '/mean_func.nii.gz',str(path_pxRegDir) + '/standard.nii.gz')
    else:
        print(' -- Directory NOT found' + str(path_pxRegDir))

    # input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
