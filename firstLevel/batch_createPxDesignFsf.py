# This script copies and adapts a specified feat designName-fsf file from patricipant examplePx_pxID for the other subjects, and runs it
# Origninal code from Marco Whitman
# Retrieved from PERL lab (November 2019): /Volumes/MRIDATA/data/CATH/1. PERL General Resources/1.5. Neuroimaging analysis - relevant documents/2.9 Scripts/Faces task/run_1stlevel_feat_patients.txt
# Updated to python by Cassandra Gould van Praag, November 2019

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# https://pbpython.com/pathlib-intro.html
from pathlib import Path
import time
import numpy as np
import subprocess

# h2mri: edit the below to point to your sourcedata and code folder
dataRoot ='/vols/Scratch/brc_em/7DP/sourcedata' #where the persons you want to make the file for are
dataRoot = Path(dataRoot)
codeRoot ='/vols/Scratch/brc_em/7DP/code_AdeC'
designRoot = os.path.join(codeRoot,'firstLevel','design')

# h2mri: edit the below to match your data
# h2mri: Example pxID is the one you used to create the manual design file with the FEAT gui
# h2mri: designName is the design you created with the gui and saved in this code folder
# h2mri: designRoot is the new directory that will hold the design files for each particpant
examplePx_pxID = 'sub-101'
task = 'faces'
# designName = 'design_firstLevel_task-' + task
# this is now moved to below
if not os.path.isdir(designRoot):
    print(' -- Creating feat design directory: ' + designRoot)
    os.makedirs(designRoot)
else:
    print(' -- Using feat design directory: ' + designRoot)

# print(' -- Name of designs to be created for new participants: ' + designName)
# this has also been moved to below

# these should remain fixed for all studies if following my instructions + BIDS
subIdentifier = 'sub-'
path_exampleDesignFile = 'firstLevel_task-faces_examplePx101_test.fsf'
path_examplePx_fmriprepProcessedData = os.path.join(dataRoot,'fmriprep','fmriprep',examplePx_pxID,'func',examplePx_pxID + '_task-' + task + '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz')

# get the output of fslval dim4 (nVolumes) and replace white spaces
str_bash = 'fslval '  + path_examplePx_fmriprepProcessedData + ' dim4 | sed \'s/ //g\''
# https://stackoverflow.com/questions/1410976/equivalent-of-bash-backticks-in-python
# https://stackoverflow.com/questions/36422572/python-subprocess-output-without-n
# The retruned value of dimensions is in bytes.
# https://stackoverflow.com/questions/606191/convert-bytes-to-a-string
examplePx_nVols = int(subprocess.check_output(str_bash, shell = True).strip().decode("utf-8"))
print(' -- Example participant number of volumes: ' + str(examplePx_nVols))

# get the total number of voxels for each dimension (replacing white spaces)
# these values are expected to be fixed for all participants, i.e. assume acquisition sequence does not change
str_bash_d1 = 'fslval '  + path_examplePx_fmriprepProcessedData + ' dim1 | sed \'s/ //g\''
str_bash_d2 = 'fslval '  + path_examplePx_fmriprepProcessedData + ' dim2 | sed \'s/ //g\''
str_bash_d3 = 'fslval '  + path_examplePx_fmriprepProcessedData + ' dim3 | sed \'s/ //g\''
dim1 = int(subprocess.check_output(str_bash_d1, shell = True).strip().decode("utf-8"))
dim2 = int(subprocess.check_output(str_bash_d2, shell = True).strip().decode("utf-8"))
dim3 = int(subprocess.check_output(str_bash_d3, shell = True).strip().decode("utf-8"))
examplePx_totalVox=dim1*dim2*dim3*examplePx_nVols

# get a list of participant directories
for i in dataRoot.glob('fmriprep/fmriprep/*/'):
    pxID = (i.name)
    if not i.is_dir():
        # print(' -- THIS IS NOT A DIRECTORY!')
        continue
    # skips this directory/pxID if is doesn't contain the subIdentifier string ("sub-")
    if subIdentifier not in pxID:
        continue

    print('\n')
    print(' -- Processing participant: ' + pxID)

    # h2mri: put in here all your participants who should be excluded from first level analysis
    # skip creating and running the design for participant listed in the excludedParticipantsList.txt file
    # this file should sit in the same directory as this code.
    # this bit of code needs a Cass fix to do an exact string match
    with open('excludedParticipantsList.txt') as f:
        if pxID in f.read():
            print(' -- This participant is in the excluded list. Skipping this participant.')
            continue

    print(' -- Creating design.')

    designName = 'design_firstLevel_task-' + task + '_' + pxID + '.fsf'
    path_pxFeatOutput = os.path.join(dataRoot,'feat','task-' + task, pxID)
    path_pxDesign = os.path.join(designRoot,designName)

    print(' -- Name of designs to be created for new participants: ' + designName)

    # if the directory for the feat output does not exist, make it.
    if not os.path.isdir(path_pxFeatOutput):
        print(' -- Creating feat output directory: ' + path_pxFeatOutput)
        os.makedirs(path_pxFeatOutput)
    else:
        print(' -- Using feat output directory: ' + path_pxFeatOutput)

    # check that preprocessed data are available
    path_pxPreproc = os.path.join(dataRoot,'fmriprep','fmriprep',pxID,'func',pxID + '_task-' + task + '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz')
    print(' -- Looking for fmriprep preprocessed data: ' + path_pxPreproc)

    if not os.path.exists(path_pxPreproc):
        print(' -- WARNING: No fmriprep preprocessed data for this participant. Skipping this participant.')
        continue
    else:
        print(' -- Found fmriprep processed data.')

		# copy fsf-file from examplePx replace pxID and number of volumes
        str_bash = 'fslval '  + path_pxPreproc + ' dim4 | sed \'s/ //g\''
        nVols = int(subprocess.check_output(str_bash, shell = True).strip().decode("utf-8"))
        totalVox = dim1*dim2*dim3*nVols

        # https://stackoverflow.com/questions/4128144/replace-string-within-file-contents
        # https://note.nkmk.me/en/python-str-replace-translate-re-sub/
        with open(path_exampleDesignFile, "rt") as fin:
            with open(path_pxDesign, "wt") as fout:
                for line in fin:
                    fout.write(line.replace(examplePx_pxID, pxID).replace(str(examplePx_nVols),str(nVols)).replace(str(examplePx_totalVox),str(totalVox)))

        print(' -- New design saved as: ' + path_pxDesign)

	    # RUN feat:
        print(' -- Running FEAT: ' + path_pxDesign)
        str_bash = 'feat ' + path_pxDesign
        process = subprocess.run(str_bash, shell = True)
