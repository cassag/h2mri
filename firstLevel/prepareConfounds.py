# prepareConfounds.py

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# https://pbpython.com/pathlib-intro.html
from pathlib import Path
import time
import numpy as np

# Supressing the "SettingWithCopyWarning" which comes when we summ the non_steady_state_outlier columns
pd.options.mode.chained_assignment = None  # default='warn'

# h2mri: edit the below to point to your sourcedata folder
# dataRoot = '/vols/Scratch/brc_em/SDLamo/sourcedata/'
dataRoot = '/vols/Scratch/brc_em/7DP/sourcedata/'
# dataRoot = '/Users/cassandragouldvanpraag/Documents/mount/scratch/cassag/mycode/h2mri/firstLevel/sourcedata'
dataRoot = Path(dataRoot)

# h2mri: edit the below to match the task you are modeling. This should match the name of the task that was assigned in BIDS
task = 'faces'

# for identifying directories which are participant data, cf logs etc.
subIdentifier = 'sub-'

# glob for directories only, i.e. the list of participants who have been fmriprep'd
for i in dataRoot.glob('fmriprep/fmriprep/*/'):
    # print(i)
    pxID = (i.name)
    # print(i.name)
    if not i.is_dir():
        # print(' -- THIS IS NOT A DIRECTORY!')
        continue
    # skips this directory/pxID if is doesn't contain the subIdentifier string ("sub-")
    if subIdentifier not in pxID:
        continue
    print('\n')
    print(' -- Processing participant: ' + pxID)
    # the path_pxConfounds should match this structure as created by fmriprep
    path_pxConfounds = i.joinpath('func',pxID + '_task-' + task + '_desc-confounds_regressors.tsv')
    if Path.exists(path_pxConfounds):
        print(' -- Confounds file: ' + str(path_pxConfounds))
    else:
        print(' -- No Confounds file')
    # read in the confounds file
    pxConfounds = pd.read_csv(path_pxConfounds,sep='\t')
    # tmp=list(pxConfounds.columns.values)
    # for item in tmp:
    #     print(item)

    if 'non_steady_state_outlier00' not in pxConfounds.columns:
        print(' -- Non steaty state vols found: 0')
        print(' -- Generating dummy non steady state regressor')

        # if none found, insert a new colun at the start of the dataframe all with the value 0
        pxConfounds.insert(0,'non_steady_state_outlier', 0)
    else:
        # get all the columns which start with non_steady_state_outlier, sum across all columns then add that as a new column to the original dataframe
        nsso = pxConfounds.filter(regex='^non_steady_state_outlier')
        tmp=list(nsso.columns.values)
        print(' -- Non steaty state vols found: ' + str(len(tmp)))
        print(' -- Merging non_steady_state_outlier column(s) into single regressor')
        nsso['non_steady_state_outlier'] = nsso.sum(axis=1)
        # inserting the new column at the start of the dataframe so it is always first in the regressor confounds
        pxConfounds.insert(0,'non_steady_state_outlier', nsso.non_steady_state_outlier)

    # select the confounds we want (https://stackoverflow.com/questions/34682828/extracting-specific-selected-columns-to-new-dataframe-as-a-copy)
    # first check they are all there!
    myConfounds = ['non_steady_state_outlier', 'framewise_displacement', 'trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']
    if  pd.Series(myConfounds).isin(pxConfounds.columns).all():
        print(' -- All required confounds found')
        pxConfounds_selected = pxConfounds.filter(myConfounds,axis=1)
    else:
        print(' -- !! WARNING: Not all required confounds were available for this participant!')
        print(' -- !! WARNING: Skipping this participant!')
        continue

    # print(pxConfounds_selected)

    # input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')

    # replace NaN (first fd value) with 0 (https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html)
    # check how many nans (https://stackoverflow.com/questions/34537048/how-to-count-nan-values-in-a-pandas-dataframe/34537125)
    countNa = sum(len(pxConfounds_selected) - pxConfounds_selected.count())
    # print(countNa)
    if countNa == 1:
        pxConfounds_selected = pxConfounds_selected.fillna(0)
    else:
        print(' -- WARNING: I found more than one nan value in the confounds data. Dont know how to handle this!')
        print(' -- !! WARNING: Skipping this participant!')
        continue

    # print(pxConfounds_selected)
    # new filename to save the selected confounds for FEAT
    path_pxConfounds_selected = i.joinpath('func',pxID + '_task-' + task + '_desc-confounds_regressors_forFirstLevel.txt')
    if Path.exists (path_pxConfounds_selected):
        os.remove(path_pxConfounds_selected)
        print (' -- Existing confounds file has been found. This will be replaced by this new version.')
    print(' -- Saving selected confounds to: ' + str(path_pxConfounds_selected))

    # save selected confounds as txt without column headers (https://www.geeksforgeeks.org/saving-a-pandas-dataframe-as-a-csv/)
    pxConfounds_selected.to_csv(path_pxConfounds_selected, header=None, index=None, sep='\t', mode='a')
