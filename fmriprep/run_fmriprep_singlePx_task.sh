# run fmriprep
# https://fmriprep.readthedocs.io/en/stable/installation.html

# singularity image pulled 15/05/19 with:
# singularity pull --name singularityContainer_fmriprep-v140a1.simg docker://poldracklab/fmriprep:latest

# submitting job with the name "fmriprep" to the bigmem queue
# the first two lines bind the root ("base") (line 21) to make available to the singularity container (line 22)
# next two lines specify the "data" (BIDS) (line 23) and the "output" directories for the qc results (line 24)
# "participant" specifies the qc type - all participants listed will be processed (line 25)
# the "--participant-label" flag takes a list of pxIDs for preprocessing, expected to be in the BIDS directory

# h2mri: Update the bind path for "base" and the location of the singularityContainer
# h2mri: ensure the "data and "output" directories are correct for your file structure
# h2mri: ensure the particpant id is correct for participants you would like to preprocess in your BIDS directory
# h2mri: ensure the task-id is correct for the task data you would like to preprocces, or leave this flag out if you would like to proeprocess all data in your BIDS directory.
# h2mri: ensure the fs-licence-file is pointing to your code folder

fsl_sub -N fmriprep -q bigmem.q \
singularity run --cleanenv -B \
    /vols/Scratch/cassag/:/base \
    /vols/Scratch/cassag/mycode/h2mri/fmriprep/singularityContainer_fmriprep-v140a1.simg \
    /base/BIDSForWINIT/2017_102/BIDS \
    /base/BIDSForWINIT/2017_102/fmriprep \
    participant \
    --fs-license-file /base/mycode/h2mri/fmriprep/license.txt \
    --participant-label 2017102002 \
    --task-id checkerboard \
    --ignore slicetiming \
    --bold2t1w-dof 12 --force-bbr \
    --write-graph
