# run mriqc
# https://mriqc.readthedocs.io/en/stable/docker.html?highlight=docker#explaining-the-mriqc-docker-command-line

# singularity image pulled 12/08/19 with:
# singularity pull --name singularityContainer_mriqc_0152rc1.sif docker://poldracklab/mriqc:latest

# from code folder in terminal, you can verify container with below to get version number (0.15.2rc1):
# singularity run singularityContainer_mriqc_0152rc1.sif --version

# the first two lines bind the root ("base") (line 20) to make available to the singularity container (line 21)
# next two lines specify the "data" (BIDS) (line 22) and the "output" directories for the qc results (line 23)
# "participant" specifies the qc type - all participants listed will be processed (line 24)
# the "--participant-label" flag takes a list of pxIDs for qc, expected to be in the BIDS directory

# h2mri: Update the bind path for "base" and the location of the singularityContainer
# h2mri: ensure the "data and "output" directories are correct for your file structure
# h2mri: ensure the particpant id is correct for an example participant in your BIDS directory

fsl_sub -N mriqc -q bigmem.q \
singularity run --cleanenv -B \
    /vols/Scratch/cassag/:/base \
    /vols/Scratch/cassag/h2mri/mriqc/singularityContainer_mriqc_0152rc1.sif \
    /base/BIDSForWINIT/2017_102/BIDS \
    /base/BIDSForWINIT/2017_102/mriqc_v \
    participant \
    --participant_label 2017102002 \
    --verbose
