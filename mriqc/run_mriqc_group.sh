# run mriqc
# https://mriqc.readthedocs.io/en/stable/docker.html?highlight=docker#explaining-the-mriqc-docker-command-line

# singularity image pulled 12/08/19 with:
# singularity pull --name singularityContainer_mriqc_0152rc1.sif docker://poldracklab/mriqc:latest

# from code folder in terminal, you can verify container with below to get version number (0.15.2rc1):
# singularity run singularityContainer_mriqc_0152rc1.sif --version


# the first two lines bind the root ("base") (line 20) to make available to the singularity container (line 21)
# next two lines specify the "data" (BIDS) (line 22) and the "output" directories for the qc results (line 23)
# "group" specifies the qc type - all processed participants in directory will be aggregated for the group statistics (line 24)


# h2mri: Update the bind path for "base" and the location of the singularityContainer
# h2mri: ensure the "data and "output" directories are correct for your file structure

singularity run --cleanenv -B \
    /vols/Scratch/cassag/:/base \
    /vols/Scratch/cassag/BIDSForWINIT/2017_102/BIDS/code/mriqc/singularityContainer_mriqc_0152rc1.sif \
    /base/BIDSForWINIT/2017_102/BIDS \
    /base/BIDSForWINIT/2017_102/mriqc \
    group
