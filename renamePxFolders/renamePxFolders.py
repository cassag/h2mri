# renamePxDicomFolders.py
# This script reads in a csv file of participant ids which are corss referenced
# against scan ids and renams the dicom folders appropriately

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd

# where all our dicom folders are
dataRoot = '/Users/cassandragouldvanpraag/Documents/WorkICloud/Teaching/h2mri/code/temp/testDicomFolder'

# csv file containing our index of scan IDs
pxID_data = pd.read_csv('pxIDs_example.csv')
# printing so we can see a list of the column headers
# print(pxID_data)

# looping over each row in the data using the index 'p' and the iterrows function
for index, p in pxID_data.iterrows():
    # creating the old path from the root and scanID from row p
    pxFolder_old = os.path.join(dataRoot,p.scanID)
    # creating the new path from the root and pxID from row p
    pxFolder_new = os.path.join(dataRoot,str('%04d' % p.pxID))

    # Creating some on screen log of what we are doing
    print('Checking for participant directory:', p.scanID)

    # if the old directory is found (os.path.isdir returns "true")
    if os.path.isdir(pxFolder_old):
        print(' -- Found directory:',pxFolder_old)
        # added some extra spaces below so the outputs were aligned
        print(' -- Renaming to:    ', pxFolder_new)
        # rename from old to new
        os.rename(pxFolder_old, pxFolder_new)
    # if it is not found (os.path,isdir returns "false")
    else:
        print(' -- Participant directory not found.')
